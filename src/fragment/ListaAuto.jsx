import '../css/stylea.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./Header";
import Footer from './Footer';
import DataTable from 'react-data-table-component';
import { Autos } from '../hooks/Conexion';
import { borrarSesion } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
const ListaAuto = () => {
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    Autos().then((info) => {
        console.log(info);
            setData(info.data);
    });
    const columns = [
        {
            name: 'Nombre',
            selector: row => row.nombre,
        },
        {
            name: 'Modelo',
            selector: row => row.modelo,
        },
        {
            name: 'Color',
            selector: row => row.color,
        },
        {
            name: 'Precio',
            selector: row => '$ ' + row.precio,
        },
        {
            name: 'Cilindraje',
            selector: row => row.cilindraje,
        },
        {
            name: 'Estado',
            selector: row => row.estado,
        },
        {
            name: 'Identificación',
            selector: row => row.dueño_identificacion,
        },
    ];



    return (

        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}
                    <div className='container-fluid'>
                        <a className='btn btn-success' href={'/auto/registro'}>NUEVO</a>
                        <DataTable
                            columns={columns}
                            data={data}
                        />
                    </div>
                </div>
            </div>
            <Footer />
        </div>


    );
}

export default ListaAuto;