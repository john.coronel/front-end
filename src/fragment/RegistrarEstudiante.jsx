import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';

function RegistrarEstudiante() {
  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    setValidated(true);
  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row className="mb-3">
      <Form.Group as={Col} md="4" controlId="validationCustom03">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" placeholder="Nombre" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una Nombre.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom02">
        <Form.Label>Apellido</Form.Label>
          <Form.Control type="text" placeholder="Apellido" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione Apellido.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="4" controlId="validationCustom00">
        <Form.Label>Cedula</Form.Label>
          <Form.Control type="text" placeholder="Cedula" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione un Cedula.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Row className="mb-3">
        <Form.Group as={Col} md="6" controlId="validationCustom03">
          <Form.Label>Curso</Form.Label>
          <Form.Control type="text" placeholder="Curso" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una Curso.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group as={Col} md="3" controlId="validationCustom05">
          <Form.Label>Nota</Form.Label>
          <Form.Control type="text" placeholder="Nota" required />
          <Form.Control.Feedback type="invalid">
          Por favor, proporcione un Nota.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Form.Group as={Col} md="4" controlId="validationCustom06">
      <Form.Label>Materia</Form.Label>
          <Form.Control type="text" placeholder="Materia" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una Materia.
          </Form.Control.Feedback>
        </Form.Group>
      <Button className="btn btn-success mt-4" type="submit">Registrar estudiante</Button>
    </Form>
  );
}
export default RegistrarEstudiante;