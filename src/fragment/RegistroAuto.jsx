import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { GuardarAuto, ObtenerMarca } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
const RegistroAuto = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [marcaSeleccionada, setMarcaSeleccionada] = useState([])
    const [marcas, setMarcas] = useState([]);
    //acciones
    //submit
    const onSubmit = (data) => {
        var datos = {
            "nombre": data.nombre,
            "modelo": data.modelo,
            "color": data.color,
            "precio": data.precio,
            "dueño_identificacion": data.dueño_identificacion,
            "external_id":data.marcaSeleccionada
        };
        GuardarAuto(datos, getToken()).then((info) => {
            if (info.error === true) {
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.message);
                navegation('/auto');
            }
        }
        );
        
    };
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de autos!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('nombre', { required: true })} className="form-control form-control-user" placeholder="Ingrese el nombre" />
                                        {errors.nombre && errors.nombre.type === 'required' && <div className='alert alert-danger'>Ingrese un nombre</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el color" {...register('color', { required: true })} />
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'>Ingrese un color</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el precio" {...register('precio', { required: true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/ })} />
                                        {errors.precio && errors.precio.type === 'required' && <div className='alert alert-danger'>Ingrese el precio</div>}
                                        {errors.precio && errors.precio.type === 'pattern' && <div className='alert alert-danger'>Ingrese un precio valido</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese la identificacion" {...register('dueño_identificacion', { required: true })} />
                                        {errors.dueño_identificacion && errors.dueño_identificacion.type === 'required' && <div className='alert alert-danger'>Ingrese una identificacion</div>}
                                    </div>
                                    <div className="form-group">
                                        <select className='form-control' {...register('marca', { required: true })} onChange={(e) => setMarcaSeleccionada(e.target.value)}>
                                            <option>Elija una marca</option>
                                            {marcas.map((m, i) => (
                                                <option key={i} value={m.external_id}>
                                                    {m.nombre}
                                                </option>
                                            ))}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Seleccione una marca</div>}
                                    </div>
                                    <hr />
                                    <a href={"/auto"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                </form>
                                <hr />
                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}
export default RegistroAuto;