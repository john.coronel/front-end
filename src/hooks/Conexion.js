//const URL = "http://localhost/v1/index.php";
const URLN = "http://localhost:3006/api";
export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URLN + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

export const Marcas = async (key) => {
    const cabeceras = {        
        "x-api-token": key
    };
    const datos = await (await fetch(URLN + "/marca", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const AutosCant = async (key) => {
    const cabeceras = {        
        "x-api-token": key
    };
    const datos = await (await fetch(URLN + "/auto/obtener/", {
        method: "GET",
        headers: cabeceras
    })).json();    
    return datos;
}

export const Autos = async () => {
    const datos = await (await fetch(URLN + "/auto", {
        method: "GET"
    })).json();
    //console.log(datos);
    return datos;
}

export const ObtenerMarca = async () => {
    const datos = await (await fetch(URLN + "/marca", {
        method: "GET"
    })).json();
    return datos;
}

export const GuardarAuto = async (data, key) => {
    const headers = {
        "x-api-token": key        
    };
    const datos = await (await fetch(URLN + "/auto/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}